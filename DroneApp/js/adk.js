
const CONTROLLER_REFRESH_INTERVAL = 5000; // 20; //50hz

(function() {
  var btnOpen = document.querySelector(".open");
  var btnClose = document.querySelector(".close");
  var statusLine = document.querySelector("#status");
  var serialDevices = document.querySelector(".serial_devices");
  var expander = document.querySelector("#expand");
  var connection = null;
  


  var logObj = function(obj) {
    console.log(obj);
  }

  var logSuccess = function(msg) {
    log("<span style='color: green;'>" + msg + "</span>");
  };

  var logError = function(msg) {
    statusLine.className = "error";
    statusLine.textContent = msg;
    log("<span style='color: red;'>" + msg + "</span>");
  };

  var log = function(msg) {
    console.log(msg);
  };

  var changeTab = function() {
    var _in = document.querySelector("#basiclink");
    var _out = document.querySelector("#detaillink");
    if (window.location.hash === "#detaillink") {
      _out.className = "";
      _in.className = "hidden";
    } else {
      _in.className = "";
      _out.className = "hidden";
    }
  };

  var init = function() {
    if (!serial_lib)
      throw "You must include serial.js before";

    enableOpenButton(true);
    btnOpen.addEventListener("click", openDevice);
    btnClose.addEventListener("click", closeDevice);
    window.addEventListener("hashchange", changeTab);
    document.querySelector(".refresh").addEventListener("click", refreshPorts);
    initADKListeners();
    refreshPorts();
  };

  var initADKListeners = function() {
	//link sliders to output span
	document.querySelector("#roll").addEventListener('change',function(e){
		document.querySelector("#roll_value").textContent = document.querySelector("#roll").value;
	});
	document.querySelector("#yaw").addEventListener('change',function(e){
		document.querySelector("#yaw_value").textContent = document.querySelector("#yaw").value;
	});
	document.querySelector("#yaw").addEventListener('change',function(e){
		document.querySelector("#yaw_value").textContent = document.querySelector("#yaw").value;
	});
	document.querySelector("#pitch").addEventListener('change',function(e){
		document.querySelector("#pitch_value").textContent = document.querySelector("#pitch").value;
	});
	document.querySelector("#throttle").addEventListener('change',function(e){
		document.querySelector("#throttle_value").textContent = document.querySelector("#throttle").value;
	});
	document.querySelector("#aux1").addEventListener('change',function(e){
		document.querySelector("#aux1_value").textContent = document.querySelector("#aux1").value;
	});
	document.querySelector("#aux2").addEventListener('change',function(e){
		document.querySelector("#aux2_value").textContent = document.querySelector("#aux2").value;
	});
	document.querySelector("#aux3").addEventListener('change',function(e){
		document.querySelector("#aux3_value").textContent = document.querySelector("#aux3").value;
	});
	document.querySelector("#aux4").addEventListener('change',function(e){
		document.querySelector("#aux4_value").textContent = document.querySelector("#aux4").value;
	});
    setInterval(function() { sendControllerData(); }, CONTROLLER_REFRESH_INTERVAL);
  };
  
  var sendControllerData = function(){
	if(multiwii_serial_lib == null)
	{
		logError("Multiwii Lib not loaded.");
		return;
	}
	
	//break when there is no connection
	if(connection == null)
	{
		return;
	}
	
	//try to send an update
	var rcData = new ArrayBuffer(16);
	var rcDataView = new DataView(rcData);
	rcDataView.setUint16(0, document.querySelector("#roll").value, true);
	rcDataView.setUint16(2, document.querySelector("#pitch").value, true);
	rcDataView.setUint16(4, document.querySelector("#yaw").value, true);
	rcDataView.setUint16(6, document.querySelector("#throttle").value, true);
	rcDataView.setUint16(8, document.querySelector("#aux1").value, true);
	rcDataView.setUint16(10, document.querySelector("#aux2").value, true);
	rcDataView.setUint16(12, document.querySelector("#aux3").value, true);
	rcDataView.setUint16(14, document.querySelector("#aux4").value, true);
	//multiwii_serial_lib.sendMSG(connection, MSP_SET_RAW_RC, rcDataView);
	multiwii_serial_lib.sendMSG(connection, MSP_STATUS, null);
  };

  var addListenerToElements = function(eventType, selector, listener) {
    var addListener = function(type, element, index) {
      element.addEventListener(type, function(e) {
        listener.apply(this, [e, index]);
      });
    };
    var elements = document.querySelectorAll(selector);
    for (var i = 0; i < elements.length; ++i) {
      addListener(eventType, elements[i], i);
    }
  };

  var toHexString = function(i) {
    return ("00" + i.toString(16)).substr(-2);
  };

  var enableOpenButton = function(enable) {
    btnOpen.disabled = !enable;
    btnClose.disabled = enable;
  };

  var refreshPorts = function() {
    while (serialDevices.options.length > 0)
      serialDevices.options.remove(0);

    serial_lib.getDevices(function(items) {
      logSuccess("got " + items.length + " ports");
      for (var i = 0; i < items.length; ++i) {
        var path = items[i].path;
        serialDevices.options.add(new Option(path, path));
        if (i === 1 || /usb/i.test(path) && /tty/i.test(path)) {
          serialDevices.selectionIndex = i;
          logSuccess("auto-selected " + path);
        }
      }
    });
  };

  var openDevice = function() {
    var selection = serialDevices.selectedOptions[0];
    if (!selection) {
      logError("No port selected.");
      return;
    }
    var path = selection.value;
    statusLine.classList.add("on");
    statusLine.textContent = "Connecting";
    enableOpenButton(false);
    serial_lib.openDevice(path, onOpen);
  };

  var onOpen = function(newConnection) {
    if (newConnection === null) {
      logError("Failed to open device.");
      enableOpenButton(true);
	  expander.className = expander.className.replace('connected','disconnected');
      return;
    }
    connection = newConnection;
    connection.onReceive.addListener(onReceive);
    connection.onError.addListener(onError);
    connection.onClose.addListener(onClose);
    logSuccess("Device opened.");
    enableOpenButton(false);
	expander.className = expander.className.replace('disconnected','connected');
    statusLine.textContent = "Connected";
  };

  var onError = function(errorInfo) {
    if (errorInfo.error !== 'timeout') {
      logError("Fatal error encounted. Dropping connection.");
      closeDevice();
    }
  };

  var onReceive = function(data) {
	  multiwii_serial_lib.readMSG(data);
  };

  var closeDevice = function() {
   if (connection !== null) {
     connection.close();
   }
  };

  var onClose = function(result) {
    connection = null;
    enableOpenButton(true);
    statusLine.textContent = "Hover here to connect";
    statusLine.className = "";
	expander.className = expander.className.replace('connected','disconnected');
  }

  init();
})();

