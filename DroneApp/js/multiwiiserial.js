const MSP_HEADER = "$M<";
const MSP_IDENT = 100;
const MSP_STATUS = 101;
const MSP_RAW_IMU = 102;
const MSP_SERVO = 103;
const MSP_MOTOR = 104;
const MSP_RC = 105;
const MSP_RAW_GPS = 106;
const MSP_COMP_GPS = 107;
const MSP_ATTITUDE = 108;
const MSP_ALTITUDE = 109;
const MSP_BAT = 110;
const MSP_RC_TUNING = 111;
const MSP_PID = 112;
const MSP_BOX = 113;
const MSP_MISC = 114;
const MSP_MOTOR_PINS = 115;
const MSP_BOXNAMES = 116;
const MSP_PIDNAMES = 117;
const MSP_SET_RAW_RC = 200;
const MSP_SET_RAW_GPS = 201;
const MSP_SET_PID = 202;
const MSP_SET_BOX = 203;
const MSP_SET_RC_TUNING = 204;
const MSP_ACC_CALIBRATION = 205;
const MSP_MAG_CALIBRATION = 206;
const MSP_SET_MISC = 207;
const MSP_RESET_CONF = 208;
const MSP_SELECT_SETTING = 210;
const MSP_BIND = 240;
const MSP_EEPROM_WRITE = 250;
const MSP_DEBUGMSG = 253;
const MSP_DEBUG = 254;

/**
 * Reader for the multiwii protocol
 */
var MultiwiiReader = (function() {

	const IDLE = 0;
	const HEADER_START = 1;
	const HEADER_M = 2;
	const HEADER_ARROW = 3;
	const HEADER_SIZE = 4;
	const HEADER_CMD = 5;
	const HEADER_ERR = 6;
	const READ_ERR = 7;
	
	var mListener = null;
	
	var c_state = IDLE;
	var p = 0;
	var inBuf = new ArrayBuffer(256);
	var inBufView = new DataView(inBuf);
	var err_rcvd = false;
	
	var checksum = 0;
	var cmd = -1;
	var offset = 0;
	var dataSize = 0;
	
	/**
	 * Call this for each block of data received
	 * @param data of type ArrayBuffer
	 */
	var onData = function (data)
	{
		var view = new DataView(data);
		for(var i = 0; i < data.byteLength; i++)
		{
			onByte(view.getUint8(i));
		}
	}
	
	/**
	 * Parse incomming bytes and do callbacks if a command is processed, call this for each byte received in the receiving order.
	 * @param c, the next unsigned byte to parse
	 */ 
	var onByte= function (c)
	{
		if (c_state == IDLE) {
			c_state = (c == '$'.charCodeAt(0)) ? HEADER_START : IDLE;
		}
		else if (c_state == HEADER_START) {
			c_state = (c == 'M'.charCodeAt(0)) ? HEADER_M : IDLE;
		}
		else if (c_state == HEADER_M) {
			if (c == '>'.charCodeAt(0)) {
				c_state = HEADER_ARROW;
			} else if (c == '!'.charCodeAt(0)) {
				c_state = HEADER_ERR;
			} else {
				c_state = IDLE;
			}
		}
		else if (c_state == HEADER_ARROW || c_state == HEADER_ERR) {
			/* is this an error message? */
			err_rcvd = (c_state == HEADER_ERR);
			dataSize = c;
			/* reset index variables */
			p = 0;
			offset = 0;
			checksum = 0;
			checksum ^= c;
			/* the command is to follow */
			c_state = HEADER_SIZE;
		}
		else if (c_state == HEADER_SIZE) {
			cmd = c;
			checksum ^= c;
			c_state = HEADER_CMD;
		}
		else if (c_state == HEADER_CMD && offset < dataSize) {
			checksum ^= c;
			inBufView.setUint8(offset++, c);
		}
		else if (c_state == HEADER_CMD && offset >= dataSize) {
			/* compare calculated and transferred checksum */
			if (checksum == c) {
				if (err_rcvd) {
					// System.err.println("Copter did not understand request type "+c);
				} else {
					/* we got a valid response packet, evaluate it */
					evaluateCommand();
				}
			} else {
				//System.out.println("invalid checksum for command "
				//		+ ((int) (cmd & 0xFF)) + ": " + (checksum & 0xFF)
				//		+ " expected, got " + (int) (c & 0xFF));
				//System.out.print("<" + (cmd & 0xFF) + " "
				//		+ (dataSize & 0xFF) + "> {");
				//for (int i = 0; i < dataSize; i++) {
				//	if (i != 0) {
						//System.err.print(' ');
				//	}
					//System.out.print((inBuf[i] & 0xFF));
				//}
				//System.out.println("} [" + c + "]");
				//System.out.println(new String(inBuf, 0, dataSize));
			}
			c_state = IDLE;
		}
	}
	
	/**
	 * Set a listener for events that come back from the copter.
	 * @param listener
	 */
	var setListener = function(listene)
	{
		mListener = listener;
	}
	
	/**
	 * Called when a command is processed successfully.
	 * Use the internal buffer and variables to perform the callback. 
	 */
	var evaluateCommand = function()
	{
		//http://www.multiwii.com/wiki/index.php?title=Multiwii_Serial_Protocol
		//only if somebody is acutally interested
		//if(mListener != null)
		{
			if(cmd == MSP_IDENT)
			{
				var version = inBufView.getUint8(0);
				var multitype = inBufView.getUint8(1);
				var capability = inBufView.getUint32(2);
			}
			else if(cmd == MSP_STATUS)
			{
				var cycletime = inBufView.getUint16(0);
				var ic2ErrorCount = inBufView.getUint16(2);
				var sensor = inBufView.getUint16(4);
				var flag = inBufView.getUint32(6);
				var globalConfCurrentSet = inBufView.getUint8(10);
			}
			else if(cmd == MSP_RAW_IMU)
			{
				var accx = inBufView.getInt16(0);
				var accy = inBufView.getInt16(2);
				var accz = inBufView.getInt16(4);
				var gyrx = inBufView.getInt16(6);
				var gyry = inBufView.getInt16(8);
				var gyrz = inBufView.getInt16(10);
				var magx = inBufView.getInt16(12);
				var magy = inBufView.getInt16(14);
				var magz = inBufView.getInt16(16);
			}
		}
	}
	
	return {
		"onData": onData
	};
}());

var multiwii_serial_lib = (function() {
	
	/**
	 * Read from serial connection and perform callbacks.
	 * @param data, ArrayBuffer
	 */
	var readMSG = function(data)
	{
		MultiwiiReader.onData(data);
	}
	
	/**
	 * Send a message using serial connection.
	 * @param connection, serial connection object
	 * @param msp, MSP code
	 * @param payload, DataView (littleEndian true)
	 */
	var sendMSG = function(connection, msp, payload)
	{
		if (msp < 0) {
			return;
		}
		if(connection == null)
		{
			return;
		}

		var checksum = 0;
		var pl_size = payload != null ? payload.byteLength : 0;
		var buffer = new ArrayBuffer(MSP_HEADER.length + pl_size + 3);
		var view = new DataView(buffer);
		
		var test = view.byteLength;
		
		var i = 0;
		for (;i < MSP_HEADER.length; ++i) {
			view.setUint8(i, MSP_HEADER.charCodeAt(i))
		}
		view.setUint8(i, pl_size);
		view.setUint8(++i, msp);
		
		checksum ^= pl_size;
		checksum ^= msp;

		if (payload != null) {
			for (var c = 0; c < payload.byteLength; c++) {
				var currentPayloadByte = payload.getUint8(c);
				view.setUint8(++i, currentPayloadByte);
				checksum ^= currentPayloadByte;
			}
		}
		view.setUint8(++i, checksum);
		
		//var x = new Int8Array(buffer, 0);
		connection.send(buffer);
	}
	
	return {
    "sendMSG": sendMSG,
    "readMSG": readMSG,
  	};
}());